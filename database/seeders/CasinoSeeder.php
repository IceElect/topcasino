<?php

namespace Database\Seeders;

use App\Models\Casino;
use App\Models\CasinoFeature;
use App\Models\DepositMethod;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use stdClass;

class CasinoSeeder extends Seeder
{
    /** 
     * @var string[] 
     */
    const CRYPTO_METHODS = ['litecoin', 'dogecoin', 'bitcoin', 'tether', 'ethereum'];

    /**
     * @var array
     */
    const METHOD_NAMES = [
        "astro" => "AstroPay",
        "banktr" => "Bank Deposit",
        "banktransf" => "Bank transfer",
        "bitcoin" => "Bitcoin",
        "cashl" => "Cashlib",
        "coins" => "Coinspayd",
        "dogecoin" => "Dogecoin",
        "ecopay" => "ecoPayz",
        "ethereum" => "Ethereum ERC20",
        "ezzew" => "eZeeWallet",
        "flex" => "Flexepin",
        "inter" => "Interkassa",
        "jeton" => "Jeton",
        "kevin" => "Kevin",
        "litecoin" => "Litecoin",
        "masterc" => "MasterCard",
        "mifinity" => "Mifinity",
        "neosurf" => "Neosurf",
        "nett" => "Neteller",
        "nods" => "Nodapay",
        "openb" => "",
        "paybank" => "PayBank",
        "paysafe" => "Paysafe card",
        "revolut" => "Revolut",
        "skrill" => "Skrill",
        "soft" => "",
        "tether" => "USDT TRC/ERC20",
        "visa" => "VISA",
        "volt" => "Volt",
    ];

    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $data = Storage::disk('data')->get('casino.json');
        $data = json_decode($data);

        foreach ($data as $key => $item) {
            $this->makeCasino($item);
        }
    }

    /**
     * @param \stdClass $item
     * 
     * @return Casino
     */
    private function makeCasino(\stdClass $item): Casino
    {
        $alias = Str::kebab($item->name);
        $casino = Casino::firstWhere('alias', $alias);
        if ($casino instanceof Casino) {
            return $casino;
        }

        return DB::transaction(function () use ($item, $alias) {

            /** @var Casino $casino */
            $casino = Casino::create([
                'logo' => $item->logo,
                'logo_large' => $item?->logoLarge ?? null,
                'logo_square' => $item->logoSquare ?? null,

                'alias' => $alias,
                'name' => $item->name,
                'marker' => $item->marker,
                'bonus' => $item->bonus,
                'additional_bonus' => $item->additionalBonus,

                'rating_value' => $item->ratingValue,
                'rating_votes' => $item->ratingVotes,

                'link' => $item->link,
            ]);

            if (!empty($item->features)) {
                foreach($item->features as $key => $feature) {
                    $this->makeFeature($casino, $feature);
                }
            }

            if (!empty($item->methods)) {
                foreach($item->methods as $key => $method) {
                    $casino->depositMethods()->attach($this->makeDepositMethod($method));
                }
            }

            return $casino;
        });
    }

    /**
     * @param \stdClass $item
     * 
     * @return DepositMethod
     */
    private function makeDepositMethod(string $alias): DepositMethod
    {
        $method = DepositMethod::firstWhere('alias', $alias);
        if ($method instanceof DepositMethod) {
            return $method;
        }

        return DepositMethod::create([
            'name' => static::METHOD_NAMES[$alias],
            'alias' => $alias,
            'icon' => "methods/{$alias}.svg",
            'type' => in_array($alias, static::CRYPTO_METHODS) ? DepositMethod::TYPE_CRYPTO : DepositMethod::TYPE_FIAT,
        ]);
    }

    /**
     * @param \stdClass $item
     * 
     * @return DepositMethod
     */
    private function makeFeature(Casino $casino, string $name): CasinoFeature
    {
        $feature = new CasinoFeature();
        $feature->feature = $name;
        $feature->casino()->associate($casino);
        $feature->save();

        return $feature;
    }
}
