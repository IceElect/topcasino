<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('casinos', function (Blueprint $table) {
            $table->id();

            $table->string('logo')->nullable();
            $table->string('logo_large')->nullable();
            $table->string('logo_square')->nullable();

            $table->string('name');
            $table->string('alias')->unique();
            $table->string('marker')->nullable();
            $table->string('bonus')->nullable();
            $table->string('additional_bonus')->nullable();

            $table->decimal('rating_value', 8, 2);
            $table->integer('rating_votes');

            $table->string('link')->nullable();
            $table->boolean('active')->default(true);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('casinos');
    }
};
