<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('casino_deposit_method', function (Blueprint $table) {
            $table->id();

            $table->foreignId('casino_id')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();

            $table->foreignId('deposit_method_id')
                ->cascadeOnDelete()
                ->cascadeOnUpdate();

            $table->integer('priority');

            $table->timestamps();

            $table->unique(['casino_id', 'deposit_method_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('casino_deposit_method');
    }
};
