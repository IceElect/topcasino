export const partnerLink = (link) => {
    const url = new URL(link);

    const urlParams = new URLSearchParams(window?.location?.search);
    if (urlParams.has('gclid')) {
        url.searchParams.append('external_id', urlParams.get('gclid'));
    }
    if (urlParams.has('sub8')) {
        url.searchParams.set('sub_id_8', urlParams.get('sub8'));
    }

    return url.toString()
}