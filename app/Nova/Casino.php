<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Line;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Repeater;
use Laravel\Nova\Fields\Slug;
use Laravel\Nova\Fields\Stack;
use Laravel\Nova\Fields\Tag;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Naif\ToggleSwitchField\ToggleSwitchField;

class Casino extends Resource
{
    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Entities';

    /**
     * The model the resource corresponds to.
     *
     * @var class-string<\App\Models\Casino>
     */
    public static $model = \App\Models\Casino::class;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * @var array
     */
    public static $orderBy = ['rating_value' => 'desc'];

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name', 'alias'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [
            ID::make()->sortable(),

            Avatar::make('Logo', 'logo')
                ->disk('public')
                ->path('casino')
                ->indexWidth(100)
                ->textAlign('left')
                ->squared(),

            Image::make('Logo Large', 'logo_large')
                ->hideFromIndex()
                ->disk('public')
                ->path('casino')
                ->indexWidth(100)
                ->textAlign('left')
                ->squared(),

            Image::make('Logo Square', 'logo_square')
                ->hideFromIndex()
                ->disk('public')
                ->path('casino')
                ->indexWidth(100)
                ->textAlign('left')
                ->squared(),

            Text::make('Name', 'name')
                ->rules('required', 'max:255'),

            Slug::make('Alias', 'alias')
                ->rules('required', 'max:255')
                ->creationRules('unique:casinos,alias')
                ->updateRules('unique:casinos,alias,{{resourceId}}')
                ->from('name'),

            Stack::make('Bonus', [
                Line::make('Bonus', 'bonus'),
                Line::make('Additional Bonus', 'additional_bonus')
                    ->extraClasses('opacity-50')
                    ->asSmall(),
            ])->onlyOnIndex(),

            Text::make('Bonus', 'bonus')
                ->hideFromIndex()
                ->rules('max:255'),

            Text::make('Additional Bonus', 'additional_bonus')
                ->hideFromIndex()
                ->rules('max:255'),

            Stack::make('Rating', [
                Line::make('Rating Value', 'rating_value')
                    ->extraClasses('font-bold'),
                Line::make('Rating Votes', 'rating_votes')
                    ->extraClasses('opacity-50')
                    ->asSmall(),
            ])->sortable('rating_value')->onlyOnIndex(),

            Number::make('Rating Value', 'rating_value')
                ->rules('required')
                ->hideFromIndex()
                ->sortable()
                ->step(0.01)
                ->min(0),

            Number::make('Rating Votes', 'rating_votes')
                ->rules('required')
                ->hideFromIndex()
                ->sortable()
                ->step(0)
                ->min(0),

            Text::make('Link', 'link')
                ->hideFromIndex()
                ->rules('required', 'max:255')
                ->copyable(),

            Boolean::make('Active', 'active')
                ->default(false),

            Tag::make('Deposit Methods', 'depositMethods', DepositMethod::class)
                ->displayAsList()
                ->hideFromDetail()
                ->withPreview()
                ->showCreateRelationButton(),

            Repeater::make('Features', 'features')
                ->asHasMany(CasinoFeature::class)
                ->repeatables([
                    \App\Nova\Repeater\CasinoFeature::make(),
                ]),

            HasMany::make('Features', 'features', CasinoFeature::class)
                    ->onlyOnDetail(),

            BelongsToMany::make('Deposit Methods', 'depositMethods', DepositMethod::class)
                ->fields(function () {
                    return [
                        Text::make('Priority', 'priority')
                    ];
                }),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function cards(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function filters(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function lenses(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function actions(NovaRequest $request)
    {
        return [];
    }
}
