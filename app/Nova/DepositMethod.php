<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Fields\Avatar;
use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Slug;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Http\Requests\NovaRequest;
use Outl1ne\NovaSortable\Traits\HasSortableManyToManyRows;

class DepositMethod extends Resource
{
    use HasSortableManyToManyRows;

    /**
     * The logical group associated with the resource.
     *
     * @var string
     */
    public static $group = 'Entities';

    /**
     * The model the resource corresponds to.
     *
     * @var class-string<\App\Models\DepositMethod>
     */
    public static $model = \App\Models\DepositMethod::class;

    /**
     * The number of resources to show per page via relationships.
     *
     * @var int
     */
    public static $perPageViaRelationship = 15;

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function fields(NovaRequest $request)
    {
        return [
            ID::make()->sortable(),

            Avatar::make('Icon', 'icon')
                ->disk('public')
                ->path('methods')
                ->indexWidth(39)
                ->textAlign('left'),

            Text::make('Name', 'name')
                ->rules('required', 'max:255'),

            Slug::make('Alias', 'alias')
                ->rules('required', 'max:255')
                ->creationRules('unique:deposit_methods,alias')
                ->updateRules('unique:deposit_methods,alias,{{resourceId}}')
                ->from('name'),

            Select::make('Type', 'type')
                ->filterable()
                ->rules('required')
                ->options(static::$model::TYPES)
                ->displayUsingLabels(),

            BelongsToMany::make('Casinos', 'casinos', Casino::class),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function cards(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function filters(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function lenses(NovaRequest $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Laravel\Nova\Http\Requests\NovaRequest  $request
     * @return array
     */
    public function actions(NovaRequest $request)
    {
        return [];
    }
}
