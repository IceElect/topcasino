<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class DepositMethod extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * @var string
     */
    const TYPE_FIAT = 'fiat';

    /**
     * @var string
     */
    const TYPE_CRYPTO = 'crypto';

    /**
     * @var array
     */
    const TYPES = [
        self::TYPE_FIAT => 'Fiat',
        self::TYPE_CRYPTO => 'Crypto',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'name',
        'alias',
        'type',
        'icon',
    ];

    /**
     * @return BelongsToMany
     */
    public function casinos(): BelongsToMany
    {
        return $this->belongsToMany(Casino::class)
            ->using(CasinoDepositMethod::class)
            ->withPivot(['id', 'priority']);
    }
}
