<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property 
 */
class Casino extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'logo',
        'logoLarge',
        'logoSquare',

        'name',
        'alias',
        'marker',
        'bonus',
        'additional_bonus',

        'rating_value',
        'rating_votes',

        'link',
        'active',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'rating_value' => 'float',
        'rating_votes' => 'float',
    ];

    /**
     * @return HasMany
     */
    public function features(): HasMany
    {
        return $this->hasMany(CasinoFeature::class)->orderBy('id', 'asc');
    }

    /**
     * @return BelongsToMany
     */
    public function depositMethods(): BelongsToMany
    {
        return $this->belongsToMany(DepositMethod::class)
            ->using(CasinoDepositMethod::class)
            ->orderByPivot('priority', 'asc')
            ->withPivot(['id', 'priority']);
    }
}
