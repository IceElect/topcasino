<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;

class CasinoDepositMethod extends Pivot implements Sortable
{
    use SortableTrait;

    public $primaryKey = 'id';
    public $incrementing = true;
    public $timestamps = false;

    public $sortable = [
        'order_column_name' => 'priority',
        'sort_when_creating' => true,
    ];

    /**
     * @return Builder
     */
    public function buildSortQuery()
    {
        return static::query()
            ->where('casino_id', $this->casino_id); // As we're sorting Artists belonging to a Track, we're setting this to filter using track_id
    }

    /**
     * @return BelongsTo
     */
    public function casino(): BelongsTo
    {
        return $this->belongsTo(Casino::class);
    }

    /**
     * @return BelongsTo
     */
    public function depositMethod(): BelongsTo
    {
        return $this->belongsTo(DepositMethod::class);
    }
}
