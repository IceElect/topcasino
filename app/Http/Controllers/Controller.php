<?php

namespace App\Http\Controllers;

use App\Http\Resources\CasinoResource;
use App\Models\Casino;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Inertia\Inertia;
use Inertia\Response;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;

    /**
     * @param Request $request
     * 
     * @return Response
     */
    public function home(Request $request): Response
    {
        $casino = Casino::orderBy('rating_value', 'desc')
            ->where('active', true)
            ->with('depositMethods', 'features')
            ->limit(10)
            ->get();

        return Inertia::render('Home', [
            'casino' => CasinoResource::collection($casino)
                ->toResponse($request)
                ->getData()->data
        ]);
    }
}
