<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class CasinoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'alias' => $this->alias,
            'logo' => Storage::url($this->logo),
            'logoLarge' => Storage::url($this->logo_large),
            'logoSquare' => Storage::url($this->logo_square),
            'bonus' => $this->bonus,
            'additionalBonus' => $this->additional_bonus,
            'ratingValue' => (float) $this->rating_value,
            'ratingVotes' => (float) $this->rating_votes,
            'link' => $this->link,
            'features' => CasinoFeatureResource::collection($this->whenLoaded('features')),
            'methods' => DepositMethodResource::collection($this->whenLoaded('depositMethods')),
        ];
    }
}
