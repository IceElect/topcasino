const defaultTheme = require('tailwindcss/defaultTheme')

/** @type {import('tailwindcss').Config} */
export default {
  // content: [
  //   "./Components/**/*.{js,vue,ts}",
  //   "./Layouts/**/*.vue",
  //   "./Pages/**/*.vue",
  //   "./Plugins/**/*.{js,ts}",
  //   "./app.vue",
  //   "./error.vue",
  // ],
  content: [
      './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
      './storage/framework/views/*.php',
      './resources/views/**/*.blade.php',
      './resources/js/**/*.vue',
  ],
  theme: {
    screens: {
      'landscape-mobile': { 'raw': '(orientation: landscape) and (max-width: 1024px)' },
      ...defaultTheme.screens,
    },
    extend: {
      colors: {
        main: '#7C7F92',
        white: '#FFFFFF',
        black: '#000000',
        primary: '#1499FF',
        primaryHover: '#11C6FF',
        secondary: '#F305FF',
        dark: '#1A1D35',
      },
      padding: {
        '15': '3.75rem',
      },
      backgroundColor: {
        dark: '#000418',
        success: '#56B74B',
      },
      boxShadow: {
        'block': '0px 0px 10px 3px rgba(0, 0, 0, 0.10)',
        'brand': '0px 0px 60px -30px #1499FF, 0px 0px 100px -20px rgba(20, 153, 255, 0.60)',
      },
    },
  },
  plugins: [],
}

