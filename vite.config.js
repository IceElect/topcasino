import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';
import vue from '@vitejs/plugin-vue';
import requireContext from 'rollup-plugin-require-context';
import path from 'path';

export default defineConfig({
    build: {
        chunkSizeWarningLimit: 1600,
        sourcemap: true
    },
    resolve: {
        alias: {
          "~": path.resolve(__dirname, "node_modules"),
          "@": path.resolve(__dirname, "resources/js/"),
        },
    },
    plugins: [
        requireContext(), 
        laravel({
            input: [
                'resources/scss/main.scss',
                'resources/css/app.css',
                'resources/js/app.js',
            ],
            refresh: true,
        }),
        vue({
            template: {
                transformAssetUrls: {
                    base: null,
                    includeAbsolute: false,
                },
            },
        }),
    ],
});
